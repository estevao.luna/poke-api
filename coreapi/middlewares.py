import json
from django.http import HttpResponse
from coreapi.exceptions import AppError


class AppErrorMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        return response

    def process_exception(self, request, exception):
        is_app_error = isinstance(exception, AppError)
        if is_app_error:
            content = json.dumps({"message": exception.message})
            status_code = exception.status_code

            return HttpResponse(
                content, status=status_code, content_type="application/json"
            )

        return None
