from django.urls import path, include
from poke import views

urlpatterns = [
    path(
        "v1/",
        include(
            [
                path(
                    "pokemon/", views.ListPokemonAPIView.as_view(), name="list-pokemon"
                ),
                path(
                    "pokemon/<str:name>",
                    views.DetailPokemonAPIView.as_view(),
                    name="get-pokemon",
                ),
                path("trainer/", views.TrainerAPIView.as_view(), name="create-trainer"),
                path(
                    "trainer/<int:id>",
                    views.TrainerUserAPIView.as_view(),
                    name="trainer",
                ),
                path(
                    "trainer/<int:id>/team",
                    views.TeamAPIView.as_view(),
                    name="trainer-team",
                ),
                path(
                    "trainer/team",
                    views.CreateTeamAPIView.as_view(),
                    name="create-team",
                ),
            ]
        ),
    )
]
