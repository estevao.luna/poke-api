from poke.repositories.pokemon import PokemonRepository


class DetailPokemonUseCase:
    repository = PokemonRepository()

    def __init__(self, repository=None):
        self.repository = repository or self.repository

    def execute(self, data: dict):
        return self.repository.get(data)


class ListPokemonUseCase:
    repository = PokemonRepository()

    def __init__(self, repository=None):
        self.repository = repository or self.repository

    def execute(self, data: dict):
        return self.repository.list(data)
