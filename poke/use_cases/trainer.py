from poke.repositories.trainer import TrainerRepository, TeamRepository


class CreateTrainerUseCase:
    trainer_repository = TrainerRepository()

    def __init__(self, trainer_repository=None):
        self.trainer_repository = trainer_repository or self.trainer_repository

    def execute(self, data: dict):
        return self.trainer_repository.create(data)


class DetailTrainerUseCase:
    trainer_repository = TrainerRepository()

    def __init__(self, trainer_repository=None):
        self.trainer_repository = trainer_repository or self.trainer_repository

    def execute(self, data: dict):
        return self.trainer_repository.get_detail(data)


class UpdateTrainerUseCase:
    trainer_repository = TrainerRepository()

    def __init__(self, trainer_repository=None):
        self.trainer_repository = trainer_repository or self.trainer_repository

    def execute(self, data: dict):
        return self.trainer_repository.update(data)


class DeleteTrainerUseCase:
    trainer_repository = TrainerRepository()

    def __init__(self, trainer_repository=None):
        self.trainer_repository = trainer_repository or self.trainer_repository

    def execute(self, data: dict):
        return self.trainer_repository.delete(data)


class ListTrainerUseCase:
    trainer_repository = TrainerRepository()

    def __init__(self, trainer_repository=None):
        self.trainer_repository = trainer_repository or self.trainer_repository

    def execute(self, data: dict):
        return self.trainer_repository.list(data)


class UpdateTrainerTeamUseCase:
    team_repository = TeamRepository()

    def __init__(self, team_repository=None):
        self.team_repository = team_repository or self.team_repository

    def execute(self, data: dict):
        return self.team_repository.update_team(data)


class CreateTrainerTeamUseCase:
    team_repository = TeamRepository()

    def __init__(self, team_repository=None):
        self.team_repository = team_repository or self.team_repository

    def execute(self, data: dict):
        return self.team_repository.create_team(data)


class GetTrainerTeamUseCase:
    team_repository = TeamRepository()

    def __init__(self, team_repository=None):
        self.team_repository = team_repository or self.team_repository

    def execute(self, data: dict):
        return self.team_repository.get_team(data)


class DeleteTrainerTeamUseCase:
    team_repository = TeamRepository()

    def __init__(self, team_repository=None):
        self.team_repository = team_repository or self.team_repository

    def execute(self, data: dict):
        return self.team_repository.delete_team(data)
