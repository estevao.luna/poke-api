from rest_framework import serializers


class DetailPokemonSerializer(serializers.Serializer):
    id_pokemon = serializers.IntegerField(required=False)
    name = serializers.CharField(max_length=100, required=True)
    type = serializers.CharField(max_length=100, required=False)
    weight = serializers.IntegerField(required=False)
    picture = serializers.URLField(required=False)


class ListPokemonSerializer(serializers.Serializer):
    limit = serializers.IntegerField(required=False)
    offset = serializers.IntegerField(required=False)


class RetrieverPokemonSerializer(serializers.Serializer):
    id_pokemon = serializers.IntegerField(required=False)
    name = serializers.CharField(max_length=100, required=False)
    type = serializers.CharField(max_length=100, required=False)
