from rest_framework import serializers
from poke.serializers.pokemon import RetrieverPokemonSerializer


class BaseTrainerSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100, required=False)
    age = serializers.IntegerField(required=False)
    picture = serializers.URLField(required=False)


class TrainerSerializer(BaseTrainerSerializer):
    id = serializers.IntegerField(required=False)
    team = RetrieverPokemonSerializer(many=True, required=False)


class CreateTrainerSerializer(BaseTrainerSerializer):
    pass


class TrainerTeamSerializer(serializers.Serializer):
    id_pokemon = serializers.IntegerField(required=False)
    name = serializers.CharField(max_length=100, required=False)


class CreateTrainerTeamSerializer(TrainerTeamSerializer):
    id = serializers.IntegerField(required=False)


class ListTrainerSerializer(serializers.Serializer):
    limit = serializers.IntegerField(required=False)
    offset = serializers.IntegerField(required=False)


class TrainerTeamSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    team = RetrieverPokemonSerializer(many=True, required=False)
